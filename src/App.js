import logo from './logo.svg';
import './App.css';
import { createBrowserRouter, RouterProvider,  } from 'react-router-dom';
import { SWRConfig } from 'swr';
import axios from 'axios';
import Sports from './pages/sports';
import Events from './pages/events';
import Outcomes from './pages/outcomes';

axios.defaults.baseURL = 'http://localhost:5000';

const router = createBrowserRouter([
  {
    path: "/",
    element: <div>Head over to /sports to get started!</div>,
  },
  {
    path: "/sports",
    element: <Sports />,
  },
  {
    path: "/sports/:sport_id",
    element: <Events />,
  },
  {
    path: "sports/:sport_id/events/:event_id",
    element: <Outcomes />,
  },
]);

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <RouterProvider router = {router} />
      </header>
    </div>
  );
}

export default App;
