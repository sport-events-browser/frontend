import axios from "axios";
import useSWR from "swr";
import { useParams } from "react-router-dom";

function Events () {
  let params = useParams();
  const { data, error } = useSWR('/sports/' + params.sport_id, (url) => axios(url).then(r => r.data).catch(err => {new Error(); error.info = err.response.data.message; throw error}))
  if (error) return <div>An error has occurred</div>
  if (!data) return <div>loading...</div>

  return(
    <>
      <p>Events</p>
      {data.events.map((row) =>
        <a href={'/sports/' + params.sport_id + '/events/' + row.id} key={row.id}>{row.desc}</a>
      )}
    </>
  )
}

export default Events;