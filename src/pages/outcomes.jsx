import axios from "axios";
import useSWR from "swr";
import { useParams } from "react-router-dom";

function Outcomes () {
  let params = useParams();
  const { data, error } = useSWR('/sports/' + params.sport_id + '/events/' + params.event_id, (url) => axios(url).then(r => r.data).catch(err => {new Error(); error.info = err.response.data.message; throw error}))
  if (!data && !error) return <div>loading... </div>
  if (error) return <div>An error has occurred</div>
  if (data)
  return(
    <>
      <p>Possible Outcomes</p>
      {data.map((row) =>
        <p key = {row.id}>{row.desc} Odds: {row.odds}</p>
      )}
    </>
  )
}
export default Outcomes;