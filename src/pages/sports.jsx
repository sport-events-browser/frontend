import axios from "axios";
import useSWR from "swr";

function Sports() {
  const { data, error } = useSWR('/sports', (url) => axios(url).then(r => r.data).catch(err => {new Error(); error.info = err.response.data.message; throw error}))
  if (error) return <div>An error has occurred</div>
  if (!data) return <div>loading...</div>

  return(
    <>
      <p>Sports</p>
      {data.map((row) =>
        <a href={'/sports/' + row.id} key={row.id}>{row.desc}</a>
      )}
    </>
  )
}

export default Sports;